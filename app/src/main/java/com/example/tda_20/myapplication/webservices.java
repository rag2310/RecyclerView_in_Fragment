package com.example.tda_20.myapplication;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class webservices {

    private RecyclerView oneTabRecycler;
    private RecyclerView twoTabRecycler;
    private Activity activity;
    private RecyclerView.Adapter oneAdapter;
    private RecyclerView.Adapter twoAdapter;
    private RecyclerView.Adapter validatorAdapter;
    private int indexOne;
    private int indexTwo;
    private ArrayList<String> oneList;
    private ArrayList<String> twoList;

    public webservices(Activity activity,RecyclerView oneTabRecycler, RecyclerView twoTabRecycler) {
        this.activity = activity;
        this.oneTabRecycler = oneTabRecycler;
        this.twoTabRecycler = twoTabRecycler;
        this.oneList = new ArrayList<>();
        this.twoList = new ArrayList<>();
        this.indexOne = 0;
        this.indexTwo = 0;
        oneAdapter = new MyAdapter(this.oneList);
        twoAdapter = new MyAdapter(this.twoList);
    }

    public void getWs(){

        for (int i = 0; i <100 ; i++){

            if (i%2 == 0){
                receivesUpdateRecycler(1,"PAR : " + i);
            } else {
                receivesUpdateRecycler(2,"IMPAR : " + i);
            }
        }
    }

    private void receivesUpdateRecycler(final int idRecycler, final String proceso){

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (idRecycler == 1) {
                    validatorAdapter = oneTabRecycler.getAdapter();

                    if (validatorAdapter == null) {
                        oneList.add(indexOne, proceso);
                        oneTabRecycler.setAdapter(oneAdapter);
                    } else {
                        oneList.add(indexOne, proceso);
                        oneAdapter.notifyItemInserted(indexOne);
                    }

                    indexOne++;
                } else if (idRecycler == 2){
                    validatorAdapter = twoTabRecycler.getAdapter();

                    if (validatorAdapter == null) {
                        twoList.add(indexTwo, proceso);
                        twoTabRecycler.setAdapter(twoAdapter);
                    } else {
                        twoList.add(indexTwo, proceso);
                        twoAdapter.notifyItemInserted(indexTwo);
                    }

                    indexTwo++;
                }
            }
        });

    }
}

package com.example.tda_20.myapplication;

import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button recepcion;
    private Button btn2;
    private Button btn3;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    View oneView;
    View twoView;
    private RecyclerView oneRecyclerView;
    private RecyclerView twoRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.pager);
        recepcion = findViewById(R.id.btnrecepcion);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        OneFragment oneFragment = new OneFragment();
        TwoFragment twoFragment = new TwoFragment();

        // Add Fragments to adapter one by one
        adapter.addFragment(oneFragment, "Comun");
        adapter.addFragment(twoFragment, "Itinario");

        viewPager.setAdapter(adapter);


        tabLayout = (TabLayout) findViewById(R.id.tab);
        tabLayout.setupWithViewPager(viewPager);

        recepcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click();
            }
        });
    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void click() {
        new ejecutar().execute();
    }

    private class ejecutar extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            btn2.setVisibility(View.INVISIBLE);
            btn3.setVisibility(View.INVISIBLE);
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            oneView = viewPager.getChildAt(0);
            twoView = viewPager.getChildAt(1);
            oneRecyclerView = oneView.findViewById(R.id.comun);
            twoRecyclerView = twoView.findViewById(R.id.itinerario);
        }

        @Override
        protected String doInBackground(Void... params) {

            new webservices(MainActivity.this ,oneRecyclerView, twoRecyclerView).getWs();
            return  "";
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }
}
